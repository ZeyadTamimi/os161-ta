/*
 * Driver code for airballoon problem
 */
#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>
#include <array.h>
#include <clock.h>

// Number of ropes to test.
#define NROPES 16

/*
* The design of the system is fairly straight forward, there exists 2 different
* arrays. A hook array and a stake array.
* Since Dandelion is the only one that can access the hooks array, there is a
* guaranteed 1:1 mapping between each rope and the hook. At the start, the
* nth rope is always connected to the nth hook. Thus the contents of the hook
* array are the rope structs. Additionally, in an
* effort to reduce congestion on the rope locks Dandelion keeps a cache array,
* where each entry tells him if the rope in the corresponding hook has
* been cut. When Dandelion picks a hook he checks the cache, if the rope was cut
* then he tries a different number, else he locks the rope and checks the cut
* status, if it was not cut then he sets it to cut, if it was, that just means
* Marigold beat him to it and so he does nothing. He then releases the lock and
* updates the cache to denote that that hook has been explored and is now cut.
*
* The stakes structure is a bit more complex: The stakes start with a guaranteed
* 1:1 mapping with the ropes, but once Lord Flowerkiller starts that is no
* longer guaranteed. Thus, the stakes array contain a stake struct. This struct
* contains a lock and a re-sizable array (array.c). This internal array 
* represents the ropes that are connected to the stake. The internal arrays
* each start with one entry, the nth stake is connected to the nth rope. The 
* lock is needed since both Lord Flowerkiller and Princess Marigold can both
* modify the connections array for a particular stake.
* 
* The algorithm for Marigold is as follows: once she picks a stake, she access
* the stake stuct in the stakes array and acquires the lock, she then checks the
* size of the array, if it 0 then she unlocks it and returns. If it isn't then
* she lock the rope at the END of the connections array. This is important
* because when items are deleted from the end of the array the overhead
* associated with memmove is less (array.c Line 133). After acquiring the lock
* on the rope, she checks to see if has been cut by Dandelion, if it has, then
* she unlocks the rope, deletes the entry of the connections array, and if
* there are still more entries attempts to check the next one. If the rope was
* not cut, then it is set to cut, unlocked and then deleted from the connections
* list. If all entires in the connections array have been cut by Dandelion, then
* Marigold just tries a different stake. (after deleting them from the
* connections array)
*
* Lord FlowerKiller is similar to Marigold but with some added complexity: He
* picks 2 stakes (source and destination). Then he mirrors Marigold movements by
* locking the source stake and searching for an uncut rope (deleting the cut
* ones along the way). However, when FlowerKiller encounters an uncut rope he
* saves its pointer and deletes it from the connections array without unlocking
* the rope's lock. He then unlocks the stake and attempts to acquire the
* destination stake,
* once he has it, he just appends the saved rope pointer to the end of the
* stake's connection array unlocks the rope, then unlocks the stake.
*
* The Balloon uses a condition variable to to wait on the global ropes_left
* variable. When either Dandelion or Marigold cut a rope they acquire a lock
* that protects the count, decrement it, and release the lock. If the count
* reaches 0 then the condition variable is signaled. When the balloon thread
* wakes up, it rechecks ropes_left and if it is 0 then the balloon flies off and
* then terminates.
*
* A condition variable and a global active_forks counter is used to ensure
* that the main thread waits for its children. Before the main thread forks, it
* sets the counter to 4, starts the 4 forks, and goes into a loop waiting on the
* condition variable to be signaled. Once each thread has printed its departing
* statement, it calls notify_main_exit, which decrements the global counter and
* notifies the main thread if the count reaches 0.
*/

////////////////////////////////////////////////////////////////////////////////
// Data type definitions
////////////////////////////////////////////////////////////////////////////////
// Data structures for rope mappings
/*
* The rope struct represents a rope and has a boolean wich represents its
* current status (cut/uncut) as well as a lock to protect the boolean. It also
* contains an integer representing the rope number.
*/
typedef struct rope
{
	struct lock *rope_lock;
	volatile bool cut;
	int rope_number;
} rope;

/*
* The stake struct represents a stake on the gorund and all the ropes that are
* connected to it. It includes an array struct of connected rope pointers as
* well a lock to protect the array struct.
*/
typedef struct stake
{
	struct lock *stake_lock;
	struct array *connected_ropes;
} stake;

////////////////////////////////////////////////////////////////////////////////
// Global data declarations
////////////////////////////////////////////////////////////////////////////////
// Global arrays for hooks, stakes, and ropes.
static int ropes_left = NROPES;
struct stake stakes[NROPES];
struct rope hooks[NROPES];

// Synchronization primitives for the hooks and stakes.
struct lock *ropes_left_lock;
struct cv *all_ropes_cut;

// Global thread management variables.
volatile int active_forks;
struct lock *active_forks_lock;
struct cv *all_forks_exit;

////////////////////////////////////////////////////////////////////////////////
// Helper Functions
////////////////////////////////////////////////////////////////////////////////
/*
* This method is responsible for atomically decrementing the global ropes_left
* counter, and notifying the balloon when its time to take off.
*/
static
bool
decrement_rope_count(void)
{
	bool ropes_cut = false;
	lock_acquire(ropes_left_lock);
	ropes_left--;
	// Check that no party is cutting more ropes than we started with.
	KASSERT(ropes_left >= 0);
	if (ropes_left == 0) {
		cv_signal(all_ropes_cut, ropes_left_lock);
		ropes_cut = true;
	}
	lock_release(ropes_left_lock);

	return ropes_cut;
}

/*
* This method is responsible for atomically checking whether or not all the
* ropes have been cut.
*/
static
bool
check_all_ropes_cut(void)
{
	bool ropes_cut = false;
	lock_acquire(ropes_left_lock);
	// Check that no party is cutting more ropes than we started with.
	KASSERT(ropes_left >= 0);
	if (ropes_left == 0)
		ropes_cut = true;
	lock_release(ropes_left_lock);
	return ropes_cut;
}

/*
* This function is responsible for atomically decreasing the number of active
* threads and signaling the main thread if all of its sub threads have exited.
*/
static
void
notify_main_exit(void)
{
	lock_acquire(active_forks_lock);
	// Decrement the active thread count and ensure that there are no errors
	// with active_forks value.
	active_forks--;
	KASSERT(active_forks >= 0);
	if (active_forks == 0)
		cv_signal(all_forks_exit, active_forks_lock);
	lock_release(active_forks_lock);
}

/*
* This function is used in cleanup to ensure that all array.c arrays are empty
* before they are destroyed.
*/
static
void
empty_connections_array(struct array *connection_array)
{
	while(array_num(connection_array) != 0)
		array_remove(connection_array, array_num(connection_array) - 1);
}


////////////////////////////////////////////////////////////////////////////////
// Threads
////////////////////////////////////////////////////////////////////////////////
static
void
dandelion(void *p, unsigned long arg)
{
	// This prevents the compiler from complaining about unused parameters.
	(void)p;
	(void)arg;

	kprintf("Dandelion thread starting\n");

	// Initialize Dandelion's Cache. Since Dandelion is the only thread that
	// operates on the hook array if he cuts a rope or discovers that it is cut,
	// he will cache this info so he never has to require the rope's lock.
	bool rope_status_cache[NROPES];
	for (int hook_number = 0; hook_number < NROPES; hook_number++)
		rope_status_cache[hook_number] = false;

	// This loop ensures that Dandelion stops if all the ropes have been cut.
	bool stop_cutting = false;
	while (!stop_cutting) {
		// Reset loop variables.
		bool rope_cut = false;
		// Dandelion first picks a random hook.
		int hook_number = random() % NROPES;
		// If Dandelion's cache indicates that the rope was cut, Then he doesn't
		// need to bother with checking it, this is because cut ropes cannot
		// be uncut.
		if (rope_status_cache[hook_number] == false) {
			// If the rope's status was not cached then Dandelion needs to lock
			// the rope on the chosen hook, sever it/check if it was severed,
			// then release the lock.
			lock_acquire(hooks[hook_number].rope_lock);
			if (!hooks[hook_number].cut) {
				hooks[hook_number].cut = true;
				rope_cut = true;
			}
			lock_release(hooks[hook_number].rope_lock);
			// Update the cache.
			rope_status_cache[hook_number] = true;
		}

		// If Dandelion manages to cut the rope then he announces it, updates
		// the global ropes_left variable, and checks if he should stop cutting.
		if (rope_cut) {
			kprintf("Dandelion severed rope %d\n", hook_number);
			stop_cutting = decrement_rope_count();
		}
		// If no rope was cut, Dandelion still needs to check if he should stop.
		else {
			stop_cutting = check_all_ropes_cut();
		}

		// To give each thread its fair share of time, Dandelion yields
		// every time he cuts a rope and there are still more ropes to cut.
		if (rope_cut && !stop_cutting)
			thread_yield();
	}

	// Dandelion announces that he is terminating, then notifies the main
	// thread.
	kprintf("Dandelion thread done\n");
	notify_main_exit();
}

static
void
marigold(void *p, unsigned long arg)
{
	// This prevent the compiler from complaining about unused parameters.
	(void)p;
	(void)arg;
	
	kprintf("Marigold thread starting\n");

	// This loop ensures that Marigold stops if all the ropes have been cut.
	bool stop_cutting = false;
	while (!stop_cutting) {
		// Reset loop variables.
		bool rope_cut = false;
		int cut_rope_number = -1;
		// First Marigold needs to select a stake to cut a rope from.
		int stake_number = random() % NROPES;
		// Next Marigold needs to acquire the stake's lock to prevent Lord
		// FlowerKiller from modifying it.
		lock_acquire(stakes[stake_number].stake_lock);
		// Marigold loops though each element in the array in a FILO order,
		// until she finds a rope to cut. If a rope is cut the it is deleted
		// from the array.
		while (!rope_cut) {
			// If there are no more ropes to check then Marigold leaves the
			// stake.
			int number_of_ropes = array_num(stakes[stake_number].connected_ropes);
			if (number_of_ropes== 0)
				break;
			
			rope * chosen_rope = array_get(stakes[stake_number].connected_ropes,
											number_of_ropes - 1);
			// Marigold then attempts to lock the last rope of the array.
			lock_acquire(chosen_rope->rope_lock);
			if (!chosen_rope->cut) {
				chosen_rope->cut = true;
				cut_rope_number = chosen_rope->rope_number;
				rope_cut = true;
			}
			lock_release(chosen_rope->rope_lock);
			// If Marigold visits a rope then it is deleted form the connections
			// array. This because, a visited rope will either be cut by
			// Marigold, or it would have already been cut by Dandelion.
			array_remove(stakes[stake_number].connected_ropes,
							number_of_ropes - 1);
		}
		// Marigold no longer needs the lock on the stake so she releases it.
		lock_release(stakes[stake_number].stake_lock);

		// If Marigold manages to cut a rope then she announces it, updates
		// the global ropes_left variable, and checks if she should stop
		// cutting.
		if (rope_cut) {
			kprintf("Marigold severed rope %d from stake %d\n", cut_rope_number,
					stake_number);
			stop_cutting = decrement_rope_count();
		}
		// If no rope was cut, Marigold still needs to check if she should stop.
		else {
			stop_cutting = check_all_ropes_cut();
		}
		// To give each thread its fair share of time, Marigold yields
		// everytime she cuts a rope and there are still more ropes to cut.
		if (rope_cut && !stop_cutting)
			thread_yield();
	}

	// Marigold announces that she is terminating, then notifies the main
	// thread.
	kprintf("Marigold thread done\n");
	notify_main_exit();
}

static
void
flowerkiller(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Lord FlowerKiller thread starting\n");

	// This loop ensures that Lord FlowerKiller stops if all the ropes have been
	// cut.
	bool stop_moving_ropes = false;
	while(!stop_moving_ropes) {
		// Reset loop variables.
		rope * rope_to_move = NULL;
		// Lord FlowerKiller first choses which stake to move a rope from
		// and where to move it to.
		int source_stake = random() % NROPES;
		// Note: The rules do not dictate that we cant move from one stake to
		// the same stake! Our system should be able to handle it.
		int destination_stake = random() % NROPES;
		// Lord FlowerKiller then ensures that the source stake still has a
		// rope connected to it. Thus he locks it and searches for an uncut
		// rope. (deleting cut ropes along the way)
		lock_acquire(stakes[source_stake].stake_lock);
		while (rope_to_move == NULL) {
			int number_of_ropes = array_num(stakes[source_stake].connected_ropes);
			// If there are no ropes on the stake then Lord FlowerKiller stops 
			// and selects another pair of stakes.
			if (number_of_ropes == 0)
				break;
			// In order to be more efficient Lord FlowerKiller always looks at
			// the end of the array, thus if an element needs to be deleted the
			// overhead of deletion is less. (array.c line 133)
			rope * chosen_rope = array_get(stakes[source_stake].connected_ropes,
											number_of_ropes - 1);
			// Lord FlowerKiller then checks if the rope should be moved, or
			// deleted (if it was cut by Dandelion). If the rope was cut, then
			// the lock is released, else the lock remains.
			lock_acquire(chosen_rope->rope_lock);
			if (!chosen_rope->cut)
				rope_to_move = chosen_rope;
			else
				lock_release(chosen_rope->rope_lock);

			array_remove(stakes[source_stake].connected_ropes,
							number_of_ropes - 1);
		}
		lock_release(stakes[source_stake].stake_lock);
		
		// If Lord Flowerkiller did find a rope to move then the destination
		// steak needs to be locked.
		if (rope_to_move != NULL) {
			lock_acquire(stakes[destination_stake].stake_lock);
			// Lord Flowerkiller the attaches the rope he was moving to the
			// stake.
			int add_status = array_add(stakes[destination_stake].connected_ropes,
										rope_to_move,
										NULL);
			KASSERT(add_status == 0);
			lock_release(stakes[destination_stake].stake_lock);
			// Finally, Lord FlowerKiller releases the lock from the rope, 
			// indicating that the move is now done.
			lock_release(rope_to_move->rope_lock);

			// This is then announced to the world.
			kprintf("Lord FlowerKiller switched rope %d from stake %d to stake %d\n",
					rope_to_move->rope_number, source_stake, destination_stake);
		}

		// Lord Flowerkiller then checks to see if all the ropes have been cut, 
		// if so he leaves the loop and terminates the thread.
		stop_moving_ropes = check_all_ropes_cut();

		// Upon a successful move and if there are still ropes left,
		// Lord FlowerKiller yields.
		if ((rope_to_move != NULL) && !stop_moving_ropes)
			thread_yield();
	}

	// Lord Flowerkiller announces that he is terminating, then notifies the 
	// main thread.
	kprintf("Lord FlowerKiller thread done\n");
	notify_main_exit();
}

static
void
balloon(void *p, unsigned long arg)
{
	// This prevent the compiler from complaining about unused parameters.
	(void)p;
	(void)arg;
	
	kprintf("Balloon thread starting\n");


	// The Balloon's job is to check if all ropes have been cut, if they havent
	// then it waits for a signal before rechecking.
	lock_acquire(ropes_left_lock);
	while (ropes_left != 0)
		cv_wait(all_ropes_cut, ropes_left_lock);
	lock_release(ropes_left_lock);

	// The Balloon announces that it has been free and that it is terminating,
	// then it notifies the main thread.
	kprintf("Balloon freed and Prince Dandelion escapes!\n");
	kprintf("Balloon thread done\n");
	notify_main_exit();
}

int
airballoon(int nargs, char **args)
{

	(void)nargs;
	(void)args;
	(void)ropes_left;
	// Initialize function variables
	int err = 0;
	// Reset global variables.
	ropes_left = NROPES;
	// Initialize global synchronization primitives.
	ropes_left_lock = lock_create("Ropes Left Lock");
	all_ropes_cut = cv_create("All ropes cut");
	active_forks_lock = lock_create("Active thread lock");
	all_forks_exit = cv_create("All threads exit");
	
	// Initialize the ropes
	// Since the mapping between a rope and a hook is always 1:1 the hook array
	// is the ropes array.
	for (int rope_number = 0; rope_number < NROPES; rope_number++) {
		hooks[rope_number].rope_lock = lock_create("Rope Lock " + rope_number);
		hooks[rope_number].cut = false;
		hooks[rope_number].rope_number = rope_number;
	}

	// Initialize the stakes array
	for (int stake_number = 0; stake_number < NROPES; stake_number++) {
		stakes[stake_number].stake_lock = lock_create("Stake Lock " + stake_number);
		stakes[stake_number].connected_ropes = array_create();
		array_init(stakes[stake_number].connected_ropes);
		int status = array_add(stakes[stake_number].connected_ropes,
							   &hooks[stake_number],
							   0);
		KASSERT(status == 0);
	}
	
	// Before creating the forks the global counter of active forks must be
	// initialized.
	active_forks = 4;

	// Begin creating the forks.
	err = thread_fork("Air Balloon",
			  NULL, balloon, NULL, 0);
	if(err)
		goto panic;

	err = thread_fork("Marigold Thread",
			  NULL, marigold, NULL, 0);
	if(err)
		goto panic;
	
	err = thread_fork("Dandelion Thread",
			  NULL, dandelion, NULL, 0);
	if(err)
		goto panic;
	
	err = thread_fork("Lord FlowerKiller Thread",
			  NULL, flowerkiller, NULL, 0);
	if(err)
		goto panic;

	goto done;
panic:
	panic("airballoon: thread_fork failed: %s)\n",
	      strerror(err));
	
done:

	// Once all threads have been created, the main thread needs to wait for all
	// the forked threads to finish.
	lock_acquire(active_forks_lock);
	while (active_forks != 0)
		cv_wait(all_forks_exit, active_forks_lock);
	lock_release(active_forks_lock);

	// Once all threads have finished, the main thread begins to clean up.
	// Clean up the ropes.
	for (int rope_number = 0; rope_number < NROPES; rope_number++) {
		lock_destroy(hooks[rope_number].rope_lock);
		KASSERT(hooks[rope_number].cut == true);
		KASSERT(hooks[rope_number].rope_number == rope_number);
	}

	// clean up the stakes array
	for (int stake_number = 0; stake_number < NROPES; stake_number++) {
		lock_destroy(stakes[stake_number].stake_lock);
		empty_connections_array(stakes[stake_number].connected_ropes);
		array_destroy(stakes[stake_number].connected_ropes);
	}

	// Clean up global synchronization primitives.
	lock_destroy(ropes_left_lock);
	cv_destroy(all_ropes_cut);
	lock_destroy(active_forks_lock);
	cv_destroy(all_forks_exit);

	// Inform the user that the main thread is exiting.
	kprintf("Main thread done\n");
	return 0;
}
