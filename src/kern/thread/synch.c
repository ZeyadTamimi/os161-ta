/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Synchronization primitives.
 * The specifications of the functions are in synch.h.
 */

#include <types.h>
#include <lib.h>
#include <spinlock.h>
#include <wchan.h>
#include <thread.h>
#include <current.h>
#include <synch.h>

////////////////////////////////////////////////////////////
//
// Semaphore.

struct semaphore *
sem_create(const char *name, unsigned initial_count)
{
        struct semaphore *sem;

        sem = kmalloc(sizeof(struct semaphore));
        if (sem == NULL) {
                return NULL;
        }

        sem->sem_name = kstrdup(name);
        if (sem->sem_name == NULL) {
                kfree(sem);
                return NULL;
        }

	sem->sem_wchan = wchan_create(sem->sem_name);
	if (sem->sem_wchan == NULL) {
		kfree(sem->sem_name);
		kfree(sem);
		return NULL;
	}

	spinlock_init(&sem->sem_lock);
        sem->sem_count = initial_count;

        return sem;
}

void
sem_destroy(struct semaphore *sem)
{
        KASSERT(sem != NULL);

	/* wchan_cleanup will assert if anyone's waiting on it */
	spinlock_cleanup(&sem->sem_lock);
	wchan_destroy(sem->sem_wchan);
        kfree(sem->sem_name);
        kfree(sem);
}

void
P(struct semaphore *sem)
{
        KASSERT(sem != NULL);

        /*
         * May not block in an interrupt handler.
         *
         * For robustness, always check, even if we can actually
         * complete the P without blocking.
         */
        KASSERT(curthread->t_in_interrupt == false);

	/* Use the semaphore spinlock to protect the wchan as well. */
	spinlock_acquire(&sem->sem_lock);
        while (sem->sem_count == 0) {
		/*
		 *
		 * Note that we don't maintain strict FIFO ordering of
		 * threads going through the semaphore; that is, we
		 * might "get" it on the first try even if other
		 * threads are waiting. Apparently according to some
		 * textbooks semaphores must for some reason have
		 * strict ordering. Too bad. :-)
		 *
		 * Exercise: how would you implement strict FIFO
		 * ordering?
		 */
		wchan_sleep(sem->sem_wchan, &sem->sem_lock);
        }
        KASSERT(sem->sem_count > 0);
        sem->sem_count--;
	spinlock_release(&sem->sem_lock);
}

void
V(struct semaphore *sem)
{
        KASSERT(sem != NULL);

	spinlock_acquire(&sem->sem_lock);

        sem->sem_count++;
        KASSERT(sem->sem_count > 0);
	wchan_wakeone(sem->sem_wchan, &sem->sem_lock);

	spinlock_release(&sem->sem_lock);
}

////////////////////////////////////////////////////////////
//
// Lock.

struct lock *
lock_create(const char *name)
{
        struct lock *lock;

        lock = kmalloc(sizeof(struct lock));
        if (lock == NULL) {
                return NULL;
        }

        lock->lk_name = kstrdup(name);
        if (lock->lk_name == NULL) {
                kfree(lock);
                return NULL;
        }

        // Instantiate the wait channel used for waiting on lock.
        lock->lk_wchan = wchan_create(lock->lk_name);
	if (lock->lk_wchan == NULL) {
		kfree(lock->lk_name);
		kfree(lock);
		return NULL;
	}

        // Create the spinlock used to protect the wait channel and other lock
        // fields.
        spinlock_init(&lock->lk_spin_lock);

        // Initialize the lock's status variables.
        lock->lk_aquired = false;
        lock->lk_holder = NULL;

        return lock;
}

void
lock_destroy(struct lock *lock)
{
        // Ensure that the supplied lock pointer is valid.
        KASSERT(lock != NULL);

        // While not a specific part of the standard, to increase program
        // robustness if the lock was acquired when we try to delete it
        // then assert.
        KASSERT(lock->lk_aquired == false);
        
        // Else delete the lock's fields followed by the lock itself.
        spinlock_cleanup(&lock->lk_spin_lock);
        wchan_destroy(lock->lk_wchan);
        kfree(lock->lk_name);
        kfree(lock);
}

void
lock_acquire(struct lock *lock)
{
        // Ensure that the supplied lock pointer is valid.
        KASSERT(lock != NULL);

        /*
        * Do not block in an interrupt handler.
        *
        * For robustness, always check, even if the acquire can actually
        * complete without blocking.
        */
        KASSERT(curthread->t_in_interrupt == false);

        // Use the lock's spinlock to protect the wait channel and avoid race
        // conditions.
        spinlock_acquire(&lock->lk_spin_lock);
        while (lock->lk_aquired == true) {
                // As with the semaphore case, there is no ordering for threads
                // that are waiting on a lock. The wakeup order is not
                // guaranteed.
                wchan_sleep(lock->lk_wchan, &lock->lk_spin_lock);
        }

        // Update the lock to indicate that it has been acquired.
        lock->lk_aquired = true;
        lock->lk_holder = curthread;
        // Release the spinlock to allow other threads to interact with the
        // lock.
        spinlock_release(&lock->lk_spin_lock);
}

void
lock_release(struct lock *lock)
{
        // Ensure that the supplied lock pointer is valid.
        KASSERT(lock != NULL);

        // Again the release operation needs to be atomic to prevent any race
        // conditions between threads that are about to go to sleep.
        spinlock_acquire(&lock->lk_spin_lock);
       
        // For robustness, ensure that only the lock holder can release it.
        KASSERT(lock->lk_holder == curthread);
        
        // Update the lock's fields before waking other threads up.
        lock->lk_aquired = false;
        lock->lk_holder = NULL;
        wchan_wakeone(lock->lk_wchan, &lock->lk_spin_lock);
        // Release the spinlock to allow the recently awoken thread to
        // progress.
        spinlock_release(&lock->lk_spin_lock);
}

bool
lock_do_i_hold(struct lock *lock)
{
        // Ensure that the supplied lock pointer is valid.
        KASSERT(lock != NULL);

        bool do_i_hold;
        // For robustness acquire the lock's spinlock to prevent change of 
        // ownership while checking the value.
        spinlock_acquire(&lock->lk_spin_lock);
        do_i_hold = lock->lk_holder == curthread;
        spinlock_release(&lock->lk_spin_lock);
        
        return do_i_hold;
}

////////////////////////////////////////////////////////////
//
// CV


struct cv *
cv_create(const char *name)
{
        struct cv *cv;

        cv = kmalloc(sizeof(struct cv));
        if (cv == NULL) {
                return NULL;
        }

        cv->cv_name = kstrdup(name);
        if (cv->cv_name==NULL) {
                kfree(cv);
                return NULL;
        }

        cv->cv_wchan = wchan_create(cv->cv_name);
        if (cv->cv_wchan == NULL) {
                kfree(cv->cv_name);
                kfree(cv);
                return NULL;
        }

        // Create the spinlock used to protect the wait channel and other lock
        // fields.
        spinlock_init(&cv->cv_spin_lock);

        return cv;
}

void
cv_destroy(struct cv *cv)
{
        // Ensure that passed pointer is valid.
        KASSERT(cv != NULL);

        // Clean up the condition variable's fields followed up by the condition
        // variable itself.
        spinlock_cleanup(&cv->cv_spin_lock);
        wchan_destroy(cv->cv_wchan);
        kfree(cv->cv_name);
        kfree(cv);
}

void
cv_wait(struct cv *cv, struct lock *lock)
{
        // Ensure that both the condition variable and the associated lock are
        // valid.
        KASSERT(cv != NULL);
        KASSERT(lock != NULL);

        // Never block in an interrupt handler.
        KASSERT(curthread->t_in_interrupt == false);

        // While the spec doesn't enforce this, for the sake of robustness
        // ensure that the calling thread has acquired the lock before calling
        // this function.
        KASSERT(lock_do_i_hold(lock));
        
        // In order to comply with Mesa semantics the release and sleep needs
        // to be atomic, hence the spinlock. This also avoid the situation where
        // a thread attempts to wake us up but the signal is missed since
        // the thread has not yet been added to the wait channel.
        spinlock_acquire(&cv->cv_spin_lock);
        lock_release(lock);
        wchan_sleep(cv->cv_wchan, &cv->cv_spin_lock);
        spinlock_release(&cv->cv_spin_lock);
        // Once a thread leaves the wait channel the order by which it acquires
        // the lock is not important and is not guaranteed by Mesa semantics.
        lock_acquire(lock);
}

void
cv_signal(struct cv *cv, struct lock *lock)
{
        // Ensure that both the condition variable and the associated lock are
        // valid.
        KASSERT(cv != NULL);
        KASSERT(lock != NULL);

        // While the spec doesn't enforce this, for the sake of robustness
        // ensure that the calling thread has acquired the lock before calling
        // this function.
        KASSERT(lock_do_i_hold(lock));

        // Acquiring the spinlock prevents cv_signal from signaling while
        // another thread has started to sleep but has not yet placed itself
        // in the wait channel.
        spinlock_acquire(&cv->cv_spin_lock);
        wchan_wakeone(cv->cv_wchan, &cv->cv_spin_lock);
        spinlock_release(&cv->cv_spin_lock);
}

void
cv_broadcast(struct cv *cv, struct lock *lock)
{
	// Ensure that both the condition variable and the associated lock are
        // valid.
        KASSERT(cv != NULL);
        KASSERT(lock != NULL);

        // While the spec doesn't enforce this, for the sake of robustness
        // ensure that the calling thread has acquired the lock before calling
        // this function.
        KASSERT(lock_do_i_hold(lock));

        // Acquiring the spinlock prevents cv_broadcast from signaling while
        // another thread has started to sleep but has not yet placed itself
        // in the wait channel.
        spinlock_acquire(&cv->cv_spin_lock);
        wchan_wakeall(cv->cv_wchan, &cv->cv_spin_lock);
        spinlock_release(&cv->cv_spin_lock);
}
